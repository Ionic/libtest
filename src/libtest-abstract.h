/* -*- Mode: C; c-set-style: linux indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* libtest-abstract.h - Abstract Base Class for libtest

   Copyright (C) 2019 Mike Gabriel
   Copyright (C) 2019 Mihai Moldovan
   All rights reserved.

   The library test is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The library test is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Mate Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
   Boston, MA  02110-1301, USA.
 */

#ifndef libtest_abstract_h
#define libtest_abstract_h

#include <glib-object.h>

G_BEGIN_DECLS

#define LIB_TEST_TYPE_ABSTRACT (lib_test_abstract_get_type ())
G_DECLARE_DERIVABLE_TYPE (LibTestAbstract, lib_test_abstract, LIB_TEST, ABSTRACT, GObject)

struct _LibTestAbstractClass {
  GObjectClass parent_class;

  gboolean (*foo) (LibTestAbstract *self);

  /*< private >*/

  /* For further extension... */
  gpointer padding[50];
};

/* Wrapper for foo, which a purely virtual public method. */
gboolean lib_test_abstract_foo (LibTestAbstract *self);

G_END_DECLS

#endif /* libtest_abstract_h */
