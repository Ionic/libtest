/* -*- Mode: C; c-set-style: linux indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* test.c - Testing application for the library test

   Copyright (C) 2019 Mike Gabriel
   Copyright (C) 2019 Mihai Moldovan
   All rights reserved.

   The library test is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The library test is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Mate Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
   Boston, MA 02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gprintf.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "libtest-final.h"

int main (int argc, char **argv) {
  int ret = EXIT_FAILURE;

  g_printf ("Testing utility for %s version %s\n\n", PACKAGE_NAME, PACKAGE_VERSION);

  LibTestFinal *final_obj = lib_test_final_new ();

  g_printf ("Trying to call foo...\n");
  gboolean foo_ret = lib_test_final_foo (final_obj);
  g_printf ("Status: %s.\n", (foo_ret) ? "true" : "false");

  ret = foo_ret;

  return (ret);
}
