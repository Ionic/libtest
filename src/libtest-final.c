/* -*- Mode: C; c-set-style: linux indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* libtest-final.c - Final Base Class for libtest

   Copyright (C) 2019 Mike Gabriel
   Copyright (C) 2019 Mihai Moldovan
   All rights reserved.

   The library test is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The library test is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Mate Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
   Boston, MA 02110-1301, USA.
 */

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "libtest-final.h"


struct _LibTestFinal {
  LibTestAbstract parent_instance;
};

G_DEFINE_TYPE (LibTestFinal, lib_test_final, LIB_TEST_TYPE_ABSTRACT);

gboolean lib_test_final_foo (LibTestFinal *self) {
  gboolean ret = FALSE;

  g_return_val_if_fail (LIB_TEST_IS_FINAL (self), ret);
  LibTestAbstract *parent = LIB_TEST_ABSTRACT (self);
  LibTestAbstractClass *parent_class = LIB_TEST_ABSTRACT_GET_CLASS (parent);

  ret = parent_class->foo (parent);

  return (ret);
}

static gboolean lib_test_final_parent_foo (LibTestAbstract *parent) {
  gboolean ret = FALSE;

  g_return_val_if_fail (LIB_TEST_IS_FINAL (parent), ret);
  LibTestFinal *self = LIB_TEST_FINAL (parent);

  /*
   * Some actually useful code might live here.
   * We just stub it and set the return value to true.
   */
  ret = TRUE;

  return (ret);
}

static void lib_test_final_class_init (LibTestFinalClass *klass) {
  LibTestAbstractClass *parent_class = LIB_TEST_ABSTRACT_CLASS (klass);

  parent_class->foo = &lib_test_final_parent_foo;
}

static void lib_test_final_init (LibTestFinal *self) {
}

LibTestFinal* lib_test_final_new (void) {
  return (g_object_new (LIB_TEST_TYPE_FINAL, NULL));
}
