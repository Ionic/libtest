/* -*- Mode: C; c-set-style: linux indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* libtest-final.h - Final Base Class for libtest

   Copyright (C) 2019 Mike Gabriel
   Copyright (C) 2019 Mihai Moldovan
   All rights reserved.

   The library test is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The library test is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Mate Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
   Boston, MA  02110-1301, USA.
 */

#ifndef libtest_final_h
#define libtest_final_h

#include <glib-object.h>

#include "libtest-abstract.h"

G_BEGIN_DECLS

#define LIB_TEST_TYPE_FINAL (lib_test_final_get_type ())
G_DECLARE_FINAL_TYPE (LibTestFinal, lib_test_final, LIB_TEST, FINAL, LibTestAbstract)

LibTestFinal* lib_test_final_new (void);

/*
 * Wrapper for foo method in abstract base class, which a purely virtual
 * public method there.
 */
gboolean lib_test_final_foo (LibTestFinal *self);

G_END_DECLS

#endif /* libtest_final_h */
