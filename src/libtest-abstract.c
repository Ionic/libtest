/* -*- Mode: C; c-set-style: linux indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* libtest-abstract.c - Abstract Base Class for libtest

   Copyright (C) 2019 Mike Gabriel
   Copyright (C) 2019 Mihai Moldovan
   All rights reserved.

   The library test is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The library test is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Mate Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
   Boston, MA 02110-1301, USA.
 */

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "libtest-abstract.h"

G_DEFINE_ABSTRACT_TYPE (LibTestAbstract, lib_test_abstract, G_TYPE_OBJECT);

gboolean lib_test_abstract_foo (LibTestAbstract *self) {
  g_return_val_if_fail (LIB_TEST_IS_ABSTRACT (self), FALSE);
  g_return_val_if_fail (LIB_TEST_ABSTRACT_GET_CLASS (self)->foo, FALSE);

  return LIB_TEST_ABSTRACT_GET_CLASS (self)->foo (self);
}

static void lib_test_abstract_class_init (LibTestAbstractClass *klass) {
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
}

static void lib_test_abstract_init (LibTestAbstract *self) {
}
